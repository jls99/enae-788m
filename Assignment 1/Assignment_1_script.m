bag = rosbag('2021-09-22-22-40-25.bag');
bSel = select(bag,'Topic','/mavros/local_position/pose');
tsX = timeseries(bSel,'Pose.Position.X');
tsY = timeseries(bSel,'Pose.Position.Y');
tsZ = timeseries(bSel,'Pose.Position.Z');
tsX.Time = tsX.Time - tsX.Time(1);
tsY.Time = tsY.Time - tsZ.Time(1);
tsZ.Time = tsZ.Time - tsZ.Time(1);
figure;
plot(tsX);
hold on
plot(tsY);
plot(tsZ);
title('Position vs Time')
xlabel('Time [s]')
ylabel('Position [m]')
legend('X','Y','Z','Location','northeast')
